<?php

namespace app\controllers;

use app\models\Name;
use app\models\NameSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class AdminController extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
            [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return !\Yii::$app->user->isGuest && \Yii::$app->user->identity->role == 'admin';
                        }
                    ]
                ]
            ]
        ];
    }

    public function actionIndex(){
        $searchModel = new NameSearch();
        $dapaProvider = $searchModel->search(\Yii::$app->request->post());
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dapaProvider' => $dapaProvider
        ]);
    }

    public function actionConfirm($id){
        $model = $this->_getModel($id);
        $model->status = Name::STATUS_CONFIRM;
        $model->save(false);
        return $this->redirect(['admin/index']);
    }

    public function actionCancel($id){
        $model = $this->_getModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['admin/index']);
        }
        return $this->render('cancel', ['model' => $model]);
    }

    private function _getModel($id){
        $model = Name::findOne($id);
        if(!$model){
            throw new NotFoundHttpException('Модель не найдена');
        }
        return $model;
    }

}