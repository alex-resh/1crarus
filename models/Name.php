<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%name}}".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $name
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $memo
 * @property User $user
 */
class Name extends \yii\db\ActiveRecord
{

    const STATUS_ON_MODERATION = 0;
    const STATUS_CONFIRM = 1;
    const STATUS_REJECT = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%name}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['memo'], 'string'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 11, 'min' => 3],
            [['name'], 'match', 'pattern' => '/[a-z]+/i'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Имя',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'memo' => 'Причина',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->user_id = Yii::$app->user->id;
            if (Yii::$app->user->identity->role == 'user') {
                $this->status = 0;
            }
            return true;
        }
        return false;
    }

    public function getUser(){
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getStatusName(){
        $arr = [
            self::STATUS_ON_MODERATION => 'На модерации',
            self::STATUS_CONFIRM => 'Одобрено',
            self::STATUS_REJECT => 'Отменено',
        ];
        return $arr[$this->status];
    }

}
