<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\DataProviderInterface;
use yii\debug\models\search\UserSearchInterface;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property string $role
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class User extends \yii\db\ActiveRecord implements UserSearchInterface
{

    public $password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash'], 'string', 'max' => 255],
            [['username', 'password_hash', 'created_at', 'updated_at'], 'unique'],
            [['password'], 'safe'],
            [['auth_key'], 'default', 'value' => '0' ],
            [['email'], 'email'],
            [['email'], 'unique'],
            ['role', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'role' => 'Роль'
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class
            ]
        ];
    }

    public static function findByUsername($username){
        return self::find()->where(['username' => $username])->one();
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }

    public function search($params)
    {
        // TODO: Implement search() method.
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            if (self::find()->count() == 0){
                $this->role = 'admin';
            } else {
                $this->role = 'user';
            }
            $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            return true;
        }
        return false;
    }

    public function validatePassword($password){
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }


    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }
}
