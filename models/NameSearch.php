<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\Query;

class NameSearch extends Name
{

    public function rules()
    {
        return [
            [['user_id', 'name', 'status', 'created_at', 'updated_at', 'memo', 'user'], 'safe'],
        ];
    }

    public function search($params){
        $query = Name::find()
            ->where(['status' => Name::STATUS_ON_MODERATION]);
        $dataProvirer = new ActiveDataProvider([
            'query' => $query
        ]);

        if (!$this->load($params)){
            return $dataProvirer;
        }

        return $dataProvirer;
    }

    public function searchByUser($params){
        $query = Name::find()
            ->where(['user_id' => \Yii::$app->user->id]);
        $dataProvirer = new ActiveDataProvider([
            'query' => $query
        ]);

        if (!$this->load($params)){
            return $dataProvirer;
        }

        return $dataProvirer;
    }

}