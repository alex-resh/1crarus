<?php

/* @var $this yii\web\View */
/* @var $model \app\models\Name */
/* @var $form \yii\widgets\ActiveForm */

$this->title = 'My names';
?>
<div class="site-index">
    <?php $form = \yii\widgets\ActiveForm::begin() ?>
    <?= $form->field($model, 'name')->textInput() ?>
    <div class="btn-group">
        <?= \yii\helpers\Html::submitInput('Отправить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php \yii\widgets\ActiveForm::end() ?>
</div>
<?php
echo \yii\grid\GridView::widget([
    'dataProvider' => $dapaProvider,
    'columns' => [
        'user.username',
        'name',
        'updated_at:datetime',
        'statusName',
        'memo'
    ],
]);