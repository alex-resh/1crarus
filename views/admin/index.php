<?php
/* @var $this \yii\web\View */
/* @var $searchModel \app\models\NameSearch */
/* @var $dapaProvider \yii\data\ActiveDataProvider */

echo \yii\grid\GridView::widget([
    'dataProvider' => $dapaProvider,
    'columns' => [
        'user.username',
        'name',
        'updated_at:datetime',
        [
            'class' => \yii\grid\ActionColumn::class,
            'buttons' => [
                'confirm' => function ($url, $model, $key) {
                    return \yii\helpers\Html::a('Одобрить', ['admin/confirm', 'id' => $model->id]);
                },
                'cancel' => function ($url, $model, $key) {
                    return \yii\helpers\Html::a('Отменить', ['admin/cancel', 'id' => $model->id]);
                }
            ],
            'template' => '{confirm} {cancel}'
        ]
    ],
]);