<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\Name */
/* @var $form \yii\widgets\ActiveForm */
?>
<?php $form = \yii\widgets\ActiveForm::begin(['method' => 'post']) ?>
<?php echo $form->field($model, 'name')->textInput(['readonly' => true]) ?>
<?php echo $form->field($model, 'memo')->textarea()->label('Причина') ?>
<div class="btn-group">
    <?php echo \yii\helpers\Html::submitInput('Отправить') ?>
</div>
<?php \yii\widgets\ActiveForm::end() ?>
